# Let's share books

## Running the front end (development)

1. `$ cd vue-frontend`
1. `$ npm install`
1. `$ npm run dev`

## Running the backend (Python 3)

1. `$ cd sanic-backend`
1. `$ python -m venv venv`
1. `$ . ./venv/bin/activate`
1. `$ pip install -r requirements.txt`
1. `$ python motw_server.py 2018`

When you are done:

1. `$ deactivate`

## Populating the the backend

1. `$ cd python-client`
1. `$ wget https://slowrotation.memoryoftheworld.org/metadata.db`
1. Change the `db_path` on `motw_client.py` to point the just downloaded `metadata.db`
1. `$ . ./venv/bin/activate`
1. `$ pip install -r requirements.txt`
1. `$ python motw_client.py`
