import rapidjson as rjson
import sys
import time
import pathlib
from setproctitle import setproctitle
from asgiref.sync import sync_to_async
import zlib
import pickle

from sanic import Sanic
from sanic import response as rs
from sanic.views import HTTPMethodView
from sanic.views import stream as stream_decorator
from sanic.exceptions import abort
from sanic_cors import CORS

import motw
import asyncio

if len(sys.argv) > 2:
    setproctitle(sys.argv[2])
else:
    setproctitle("motw_upload")

app = Sanic()
CORS(app)
app.config.REQUEST_MAX_SIZE = 1_000_000_000

# - initial (re)load
if not pathlib.Path("unitedstates").exists():
    pathlib.Path("unitedstates").mkdir(parents=True, exist_ok=True)

motw.libraries = motw.load_libraries()

def check_library_secret(library_uuid, library_secret):
    secret = motw.load_libraries()[library_uuid]["secret"]
    print(secret)
    ret = (
        True
        if (library_secret == motw.master_secret or library_secret == secret)
        else False
    )
    if ret:
        return ret
    else:
        abort(403)

@sync_to_async
def validate_books(bookson, schema, enc_zlib):
    if enc_zlib:
        try:
            bookson = zlib.decompress(bookson).decode("utf-8")
        except zlib.error as e:
            abort(422, "Unzipping JSON failed...")

    validate = rjson.Validator(rjson.dumps(schema))
    try:
        validate(bookson)
        del validate
        return bookson
    except ValueError as e:
        print(e)
        abort(422, "JSON didn't validate.")


class Books(HTTPMethodView):
    def get(self, request, library_uuid):
        assert request.stream is None
        return text("Here you should upload, no?")

    @stream_decorator
    async def post(self, request, library_uuid):
        t = time.time()
        library_secret = request.headers.get("Library-Secret") or request.headers.get(
            "library-secret"
        )

        check_library_secret(library_uuid, library_secret)

        encoding_header = request.headers.get("library-encoding") or request.headers.get(
            "Library-Encoding"
        )
        enc_zlib = False
        if encoding_header == "zlib":
            enc_zlib = True

        assert isinstance(request.stream, asyncio.Queue)

        library_json_path = f"/tmp/{library_uuid}.json"
        library_pickle_path = f"/tmp/{library_uuid}_{library_secret}.pickle"
        if pathlib.Path(library_json_path).exists():
            pathlib.Path(library_json_path).unlink()

        library_json = open(library_json_path, "ab")
        while True:
            body = await request.stream.get()
            if body is None:
                library_json.close()
                library_json = open(library_json_path, "rb")
                if pathlib.Path(library_json_path).exists():
                    bookson = await validate_books(library_json.read(), motw.collection_schema, enc_zlib)
                    books = rjson.loads(bookson, datetime_mode=rjson.DM_ISO8601)
                    del bookson
                    library_json.close()
                    pathlib.Path(library_json_path).unlink()
                    with open(library_pickle_path, "wb") as f:
                        pickle.dump(books, f)
                    del books

                print("books uploaded in {} seconds.".format(round(time.time() - t, 3)))
                return rs.text("data.js uploaded... yay!")
            library_json.write(body)


app.add_route(Books.as_view(), "/upload/<library_uuid>")

app.run(host="0.0.0.0", port=int(sys.argv[1]), workers=1, debug=False, access_log=False)
