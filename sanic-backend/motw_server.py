import asyncio
import glob
import pathlib
import pickle
import re
import sys
import time
import urllib
import zlib
from urllib.parse import unquote_plus as unq
from xml.sax.saxutils import escape

import rapidjson as rjson
import sortedcontainers
from asgiref.sync import sync_to_async
from sanic import Sanic
from sanic import response as rs
from sanic.exceptions import SanicException, abort
from sanic.response import json, text
from sanic.views import HTTPMethodView
from sanic.views import stream as stream_decorator

# from sanic_brogz import Compress
from sanic_cors import CORS
from setproctitle import setproctitle

import motw

if len(sys.argv) > 2:
    setproctitle(sys.argv[2])
else:
    setproctitle("motw_api")

app = Sanic()
# Compress(app)
CORS(app)
app.config.REQUEST_MAX_SIZE = 1_000_000_000


def clear_cache():
    motw.books_cache.clear()
    for fpath in glob.glob("unitedstates/search_cache/*"):
        pathlib.Path(fpath).unlink()


def update_motw_books(library_uuid):
    with open(f"unitedstates/{library_uuid}", "rb") as f:
        motw.books.update(pickle.load(f))


def update_motw_indexes():
    for b in motw.books.values():
        motw.indexed_by_time.update({str(b["last_modified"]) + b["_id"]: b["_id"]})
        # motw.indexed_by_title.update({b["title_sort"] + b["_id"]: b["_id"]})
        # motw.indexed_by_pubdate.update({str(b["pubdate"]) + b["_id"]: b["_id"]})


def remove_book_from_indexes(bookid, book):
    motw.indexed_by_time.pop(f"{book['last_modified']}{bookid}", None)
    # motw.indexed_by_title.pop(f"{book['title_sort']}{bookid}", None)
    # motw.indexed_by_pubdate.pop(f"{book['pubdate']}{bookid}", None)


def empty_pickle(library_uuid):
    with open(f"unitedstates/{library_uuid}", "wb") as f:
        pickle.dump({}, f)


def pickle_books(library_uuid, pickled_books):
    with open(f"unitedstates/{library_uuid}", "wb") as f:
        pickle.dump(pickled_books, f)


def update_pickled_books(library_uuid):
    pickled_books = sortedcontainers.SortedDict()
    with (open(f"unitedstates/{library_uuid}", "rb")) as f:
        pickled_books.update(pickle.load(f))
    return pickled_books


# - initial (re)load
if not pathlib.Path("unitedstates/search_cache").exists():
    pathlib.Path("unitedstates/search_cache").mkdir(parents=True, exist_ok=True)

motw.libraries = motw.load_libraries()

for library_uuid in motw.libraries:
    t = time.time()
    if motw.libraries[library_uuid]["state"] == "on":
        if not pathlib.Path(f"unitedstates/{library_uuid}").exists():
            empty_pickle(library_uuid)
            motw.libraries[library_uuid]["state"] = "off"
            motw.dump_libraries(motw.libraries)
            continue

        update_motw_books(library_uuid)

    print(
        "initial {} updated in {} seconds.".format(
            motw.libraries[library_uuid]["url"], round(time.time() - t, 3)
        )
    )

t = time.time()
clear_cache()
update_motw_indexes()
print("initial indexes updated in {} seconds.".format(round(time.time() - t, 3)))


# - SANIC/rapidjson
def add_library_url(book):
    library = motw.libraries[book["library_uuid"]]
    book.update({"library_url": library["url"], "librarian": library["librarian"]})
    return book


def get_max_results(request):
    max_results = motw.hateoas.max_results
    if "max_results" in request.args:
        try:
            max_results = min(120, abs(int(request.args["max_results"][0])))
        except Exception as e:
            print("messy max_results was sent: {}".format(e))
    return max_results


def get_page(request, l, max_results):
    last_p = int(l / max_results + 1)
    if "page" in request.args:
        p = min(last_p, max(0, int(request.args["page"][0]) - 1))
    else:
        p = 0
    return p, last_p


def get_cache_fingerprint(request):
    cf = ""
    for p in request.path.split("/"):
        if p:
            cf += f"_{p}_"
    return cf


def check_cache(request):
    t = time.time()
    cache_fingerprint = get_cache_fingerprint(request)
    for c in motw.books_cache:
        if cache_fingerprint in c:
            print(
                "{} got from cache in {} seconds.".format(
                    cache_fingerprint, round(time.time() - t, 3)
                )
            )
            return c[cache_fingerprint]
    return False


def hateoas(l, request, title="books"):
    # print(f"REQUEST: {request}")
    # import ipdb; ipdb.set_trace()
    max_results = get_max_results(request)
    p, last_p = get_page(request, l.__len__(), max_results)

    r = {}
    books = [
        {
            k: v
            for (k, v) in b.items()
            if k
            in [
                "_id",
                "authors",
                "pubdate",
                "title",
                "formats",
                "library_uuid",
                "cover_url",
            ]
        }
        for b in l[p * max_results : (p + 1) * max_results]
    ]
    r = {
        "_items": [add_library_url(book) for book in books],
        "_links": {
            "parent": {"title": "Memory of the World Library", "href": "/"},
            "self": {"title": title, "href": request.path[1:]},
            "prev": {"title": "previous page", "href": f"{request.path}?page={p}"},
            "next": {"title": "next page", "href": f"{request.path}?page={p+2}"},
            "last": {"title": "last page", "href": f"{request.path}?page={last_p}"},
        },
        "_meta": {
            "page": p + 1,
            "max_results": max_results,
            "total": len(l),
            "status": title,
        },
    }

    if p == 1:
        r["_links"]["prev"]["href"] = f"{request.path}"
    if p == 0:
        del r["_links"]["prev"]
    if p == last_p - 1:
        del r["_links"]["next"]

    res = rjson.dumps(r, datetime_mode=rjson.DM_ISO8601).encode()
    return res


def check_library_secret(library_uuid, library_secret):
    secret = motw.load_libraries()[library_uuid]["secret"]
    ret = (
        True
        if (library_secret == motw.master_secret or library_secret == secret)
        else False
    )
    if ret:
        return ret
    else:
        abort(403)


@sync_to_async
def validate_books(bookson, schema, enc_zlib):
    if enc_zlib:
        try:
            bookson = zlib.decompress(bookson).decode("utf-8")
        except zlib.error as e:
            abort(422, f"Unzipping JSON failed: {e}")

    validate = rjson.Validator(rjson.dumps(schema))
    try:
        validate(bookson)
        del validate
        return bookson
    except ValueError as e:
        print(e)
        abort(422, "JSON didn't validate.")


@sync_to_async
def remove_books(rookson, library_uuid):
    reset = False
    bookids = rjson.loads(rookson)
    if bookids == []:
        return True, reset

    pickled_books = update_pickled_books(library_uuid)

    for bookid in bookids:
        book = motw.books[bookid]
        remove_book_from_indexes(bookid, book)

        del motw.books[bookid]
        pickled_books.pop(bookid, None)

    clear_cache()
    pickle_books(library_uuid, pickled_books)

    return True, reset


@sync_to_async
def add_books(bookson, library_uuid, load=False):
    reset = False
    t = time.time()
    if load:
        books = bookson
        print("books.pickle loaded in {} seconds.".format(round(time.time() - t, 3)))
    else:
        books = rjson.loads(bookson, datetime_mode=rjson.DM_ISO8601)
        print("books.json loaded in {} seconds.".format(round(time.time() - t, 3)))
    del bookson

    if books == []:
        return True, reset

    library_uuid_check = list(set([book["library_uuid"] for book in books]))
    if len(library_uuid_check) != 1 or library_uuid_check[0] != library_uuid:
        return False, reset

    new_book_ids = set((book["_id"] for book in books))
    t = time.time()

    if not pathlib.Path(f"unitedstates/{library_uuid}").exists():
        empty_pickle(library_uuid)

    pickled_books = update_pickled_books(library_uuid)
    old_books_ids = pickled_books.keys()
    print("pickled_books loaded in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    ids_to_add = set(new_book_ids - old_books_ids)
    for b in books:
        if b["_id"] not in ids_to_add:
            continue

        if not reset:
            reset = True

        if "series" not in b:
            b["series"] = ""

        pickled_books.update({b["_id"]: b})
    del books
    print("pickled_books updated in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    pickle_books(library_uuid, pickled_books)
    print("pickled_books dumped in {} seconds.".format(round(time.time() - t, 3)))

    # print("index/url written in {} seconds.".format(round(time.time() - t, 3)))
    return True, reset


class Books(HTTPMethodView):
    def get(self, request, verb, library_uuid):
        assert request.stream is None
        return text("Here you should upload, no?")

    @stream_decorator
    async def post(self, request, verb, library_uuid):
        t = time.time()
        library_secret = request.headers.get("Library-Secret") or request.headers.get(
            "library-secret"
        )

        check_library_secret(library_uuid, library_secret)

        encoding_header = request.headers.get(
            "library-encoding"
        ) or request.headers.get("Library-Encoding")
        enc_zlib = False
        if encoding_header == "zlib":
            enc_zlib = True

        assert isinstance(request.stream, asyncio.Queue)
        result = b""
        while True:
            body = await request.stream.get()
            if body is None:
                print("books uploaded in {} seconds.".format(round(time.time() - t, 3)))
                if verb == "add":
                    t = time.time()
                    bookson = await validate_books(
                        result, motw.collection_schema, enc_zlib
                    )
                    print(
                        "books validated in {} seconds.".format(
                            round(time.time() - t, 3)
                        )
                    )
                    if bookson:
                        t = time.time()
                        status, reset = await add_books(bookson, library_uuid)
                        if status:
                            print(
                                "books added in {} seconds.".format(
                                    round(time.time() - t, 3)
                                )
                            )
                            return json({"reset": reset})
                elif verb == "remove":
                    t = time.time()
                    bookson = await validate_books(result, motw.remove_schema, enc_zlib)

                    if bookson:
                        status, reset = await remove_books(bookson, library_uuid)
                        if status:
                            print(
                                "books removed in {} seconds.".format(
                                    round(time.time() - t, 3)
                                )
                            )
                            return json({"reset": reset})

                abort(422, f"{verb}-ing books failed!")
            result += body


@app.route("/library/<verb>/<library_uuid>")
def library(request, verb, library_uuid):
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )
    r = re.match(
        "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}",
        library_secret,
    )
    if not r or verb not in ["add", "remove", "bookids", "on", "off"]:
        abort(422, "Wrong verb, ha!")

    if verb == "add":
        if library_uuid not in motw.libraries:
            motw.libraries.update(
                {
                    library_uuid: {
                        "secret": library_secret,
                        "state": "off",
                        "url": "",
                        "librarian": "",
                    }
                }
            )
            empty_pickle(library_uuid)

            motw.dump_libraries(motw.libraries)
            return text(f"{library_uuid} added. Let's share books...")
        else:
            abort(422, "Library already added.")

    if not check_library_secret(library_uuid, library_secret):
        abort(401)

    if verb == "remove" and library_uuid in motw.libraries:
        with open(f"unitedstates/{library_uuid}", "rb") as f:
            if len(pickle.load(f)) != 0:
                abort(422, f"{library_uuid} has still some books. Remove them first.")
        del motw.libraries[library_uuid]
        motw.dump_libraries(motw.libraries)
        return text(f"{library_uuid} removed.")

    elif verb == "on" and library_uuid in motw.libraries:
        t = time.time()
        if "librarian" in request.args and request.args["librarian"][0] != "":
            librarian = request.args["librarian"][0]
            for library in motw.libraries.items():
                if library[1]["librarian"] == librarian and library_uuid != library[0]:
                    abort(422, "Library needs an *unique* librarian's name.")
            if motw.libraries[library_uuid]["librarian"] != librarian:
                motw.libraries[library_uuid]["librarian"] = librarian
                motw.libraries[library_uuid]["state"] == "off"

        if motw.libraries[library_uuid]["librarian"] == "":
            abort(422, "Library needs a librarian's name.")

        librarian = motw.libraries[library_uuid]["librarian"]
        url = motw.libraries[library_uuid]["url"]

        if "url" in request.args and request.args["url"][0] != url:
            motw.libraries[library_uuid]["url"] = request.args["url"][0]
        elif (
            motw.libraries[library_uuid]["state"] == "on"
            and request.args["url"][0] == url
            and "reset" not in request.args
        ):
            abort(
                422, f"{library_uuid} by {librarian} served at {url} is already online."
            )

        update_motw_books(library_uuid)
        update_motw_indexes()

        motw.libraries[library_uuid]["state"] = "on"
        motw.dump_libraries(motw.libraries)

        clear_cache()
        print("library ON in {} seconds.".format(round(time.time() - t, 3)))
        return text(f"{library_uuid} by {librarian} served at {url} is now online.")

    elif verb == "off" and library_uuid in motw.libraries:
        t = time.time()
        if motw.libraries[library_uuid]["state"] == "off":
            abort(422, f"{library_uuid} is not online.")

        bookids = []
        for b in motw.books.values():
            if b["library_uuid"] == library_uuid:
                bookid = b["_id"]
                bookids.append(bookid)
                book = motw.books[bookid]
                remove_book_from_indexes(bookid, book)

        for bid in bookids:
            del motw.books[bid]

        motw.libraries[library_uuid]["state"] = "off"
        motw.dump_libraries(motw.libraries)
        clear_cache()
        print("library OFF in {} seconds.".format(round(time.time() - t, 3)))
        return text(f"{library_uuid} is now offline.")

    elif verb == "bookids" and library_uuid in motw.libraries:
        bookids = [
            f"{book['_id']}___{book['last_modified']}"
            for book in motw.books.values()
            if library_uuid == book["library_uuid"]
        ]
        if bookids == []:
            try:
                with (open(f"unitedstates/{library_uuid}", "rb")) as f:
                    bookids = [
                        f"{book['_id']}___{book['last_modified']}"
                        for book in pickle.load(f).values()
                    ]
            except Exception as e:
                print(f"No cache + {e}")
                bookids = []
        return json(bookids)
    elif library_uuid not in motw.libraries:
        abort(404, f"{library_uuid} doesn't exist.")


# @app.route("/memory")
# async def get_memory(request):
#     proc = psutil.Process(os.getpid())
#     import ipdb; ipdb.set_trace()
#     return json(
#         {"memory": "{}".format(round(proc.memory_full_info().rss / 1000000.0, 2))}
#     )


@app.route("/search/<field>/<q>")
def search(request, field, q):
    def _download_links(request, field, q, ff):
        # import ipdb; ipdb.set_trace()
        t = time.time()
        cache_fingerprint = ""

        for p in request.path.split("/"):
            if p:
                cache_fingerprint += f"_{p}_"

        dl_base = "unitedstates/search_cache/"
        dl_ext = request.args["dl"][0]
        dl_filename = f"{dl_base}{cache_fingerprint}.{dl_ext}"

        if pathlib.Path(dl_filename).exists():
            return dl_filename

        metalink_files = []
        wget_urls = []
        for b in motw.books.values():
            if ff:
                f = b[field].lower()
            else:
                f = " ".join(b[field]).lower()

            if unq(q).lower() in f:
                library_url = add_library_url(b)["library_url"]
                cover = b["cover_url"]
                url = urllib.parse.quote(f"http:{library_url}{cover}").replace(
                    "%3A", ":"
                )
                metalink_files.append(
                    motw.metalink_files(escape(cover), 0, escape(url))
                )
                wget_urls.append(url.encode())

                metadata_url = url.replace("cover.jpg", "metadata.opf")
                metadata_dir = cover.replace("cover.jpg", "metadata.opf")
                metalink_files.append(
                    motw.metalink_files(escape(metadata_dir), 0, escape(metadata_url))
                )
                wget_urls.append(metadata_url.encode())

                for frm in b["formats"]:
                    url = urllib.parse.quote(
                        f"http:{library_url}{frm['dir_path']}{frm['file_name']}"
                    ).replace("%3A", ":")
                    metalink_files.append(
                        motw.metalink_files(
                            escape(f"{frm['dir_path']}{frm['file_name']}"),
                            frm["size"],
                            escape(url),
                        )
                    )
                    wget_urls.append(url.encode())

        print("metalink rendered in {} seconds.".format(round(time.time() - t, 3)))
        res = motw.metalink_header + "\n".join(metalink_files) + motw.metalink_footer
        with open(f"{dl_base}{cache_fingerprint}.meta4", "wb") as f:
            f.write(res.encode())
        with open(f"{dl_base}{cache_fingerprint}.txt", "wb") as f:
            f.write(b"\n".join(wget_urls))
        return dl_filename

    try:
        title = "search in {}".format(field)
        if field == "titles":
            field = "title"

        if field == "librarian":
            for library in motw.libraries.items():
                if unq(q).lower() == library[1]["librarian"].lower():
                    field = "library_uuid"
                    q = library[0]
            if field == "librarian":
                abort(404, "No librarian for that query.")
        if (
            "dl" in request.args
            and request.args["dl"][0] in ["txt", "meta4"]
            and field
            in [
                "title",
                "publisher",
                "series",
                "library_uuid",
                "abstract",
                "authors",
                "languages",
                "tags",
            ]
        ):
            ff = True
            if field in ["authors", "languages", "tags"]:
                ff = False
            dl_filename = _download_links(request, field, q, ff)
            if request.args["dl"] == "meta4":
                m_type = ("application/metalink4+xml",)
            else:
                m_type = "text/plain"

            return rs.file_stream(
                f"{dl_filename}",
                mime_type=f"{m_type}",
                chunk_size=1024,
                headers={
                    "Content-Disposition": 'Attachment; filename="search_{}_{}.{}"'.format(
                        field, unq(q).lower(), request.args["dl"][0]
                    )
                },
            )

        t = time.time()
        if field in ["_id", "title", "publisher", "series", "library_uuid", "abstract"]:
            res = check_cache(request)
            if not res:
                res = [
                    motw.books[bid]
                    for bid in motw.indexed_by_time.values()
                    if unq(q).lower() in motw.books[bid][field].lower()
                ][::-1]
                cache_fingerprint = get_cache_fingerprint(request)
                if cache_fingerprint not in motw.books_cache:
                    motw.books_cache.append({cache_fingerprint: res})

                print(
                    "search for {} in {} finished in {} seconds.".format(
                        unq(q).lower(), field.lower(), round(time.time() - t, 3)
                    )
                )
            return rs.raw(hateoas(res, request, title), content_type="application/json")

        elif field in ["authors", "languages", "tags"]:
            res = check_cache(request)
            if not res:
                res = [
                    motw.books[bid]
                    for bid in motw.indexed_by_time.values()
                    if unq(q).lower() in " ".join(motw.books[bid][field]).lower()
                ][::-1]
                cache_fingerprint = get_cache_fingerprint(request)
                if cache_fingerprint not in motw.books_cache:
                    motw.books_cache.append({cache_fingerprint: res})

                print(
                    "search for {} in {} finished in {} seconds.".format(
                        unq(q).lower(), field.lower(), round(time.time() - t, 3)
                    )
                )
            return rs.raw(hateoas(res, request, title), content_type="application/json")

        elif field in [
            "_lodestone",
            "_isbn",
            "_doi",
            "_amazon",
            "_doi",
            "_issn",
            "_google",
            "_lccn",
            "_md5",
            "_sha256",
            "_sha1",
            "_goodreads",
            "_barnesnoble",
            "_mobi-asin",
        ]:
            h = hateoas(
                [
                    b
                    for b in motw.books.values()
                    if {"scheme": field[1:], "code": str(q)} in b["identifiers"]
                ],
                request,
                title,
            )
            return rs.raw(h, content_type="application/json")

    except Exception as e:
        abort(404, e)


@app.route("/autocomplete/<field>/<sq>")
def autocomplete(request, field, sq):
    r = set()
    if len(sq) < 4:
        return json({})
    if field in ["titles", "publisher"]:
        if field == "titles":
            field = "title"
            if unq(sq.lower()) in ["the "]:
                return json({})
        r = [
            b[field] for b in motw.books.values() if unq(sq.lower()) in b[field].lower()
        ]
    elif field in ["authors", "tags"]:
        for bf in [b[field] for b in motw.books.values()]:
            for f in bf:
                if unq(sq.lower()) in f.lower():
                    r.add(f)
    r = {"_items": list(r)}
    return json(r)


@app.route("/books/load/<library_uuid>")
async def load_books(request, library_uuid):
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )

    check_library_secret(library_uuid, library_secret)

    t = time.time()

    pname = f"/tmp/{library_uuid}_{library_secret}.pickle"
    with open(pname, "rb") as f:
        bookson = pickle.load(f)
        pathlib.Path(pname).unlink()

    if bookson:
        status, reset = await add_books(bookson, library_uuid, True)
        del bookson
        if status:
            print("books loaded in {} seconds.".format(round(time.time() - t, 3)))
            return json({"reset": reset})
    abort(422, "Loading uploaded file failed...")


@app.route("/books")
def books(request):
    t = time.time()
    res = check_cache(request)
    if not res:
        res = [motw.books[bid] for bid in motw.indexed_by_time.values()][::-1]
        cache_fingerprint = get_cache_fingerprint(request)
        if cache_fingerprint not in motw.books_cache:
            motw.books_cache.append({cache_fingerprint: res})
        print("all books in {} seconds.".format(round(time.time() - t, 3)))
    return rs.raw(hateoas(res, request), content_type="application/json")


@app.route("/book/<book_id>")
def book(request, book_id):
    try:
        return rs.raw(
            rjson.dumps(
                add_library_url(motw.books[book_id]), datetime_mode=rjson.DM_ISO8601
            ).encode(),
            content_type="application/json",
        )
    except Exception as e:
        abort(404, e)


@app.route("/librarians/<state>")
def librarians(request, state):
    librarians = []
    for library in motw.libraries.values():
        if library["state"] == state:
            librarians.append(library["librarian"])

    r = {"_items": librarians}
    return json(r)


app.add_route(Books.as_view(), "/books/<verb>/<library_uuid>")

# asyncio.set_event_loop(uvloop.new_event_loop())
# server = app.create_server(host="0.0.0.0", port=sys.argv[1])
# loop = asyncio.get_event_loop()
# task = asyncio.ensure_future(server)

# signal(SIGINT, lambda s, f: loop.stop())

# try:
#     loop.run_forever()
# except:
#     loop.stop()


@app.exception(SanicException)
async def foo(request, exception):
    return text(f"{exception.status_code} {exception}", status=exception.status_code)


app.static("/", "../vue-frontend/dist/index.html")
app.static("/static/", "../vue-frontend/dist/static/")
app.run(host="0.0.0.0", port=int(sys.argv[1]), workers=1, debug=False, access_log=False)
# app.run(host="0.0.0.0", port=int(sys.argv[1]) )
# app.run(host="0.0.0.0")
