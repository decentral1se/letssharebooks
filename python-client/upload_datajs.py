import time
import zlib
import requests
import sys

def upload_books(host_api, library_uuid, library_secret, payload, zipit=False):
    headers = {"Library-Secret": library_secret}
    if zipit:
        t = time.time()
        headers.update({"Library-Encoding": "zlib"})
        payload = zlib.compress(payload.encode("utf-8"))
        print("zlib payload in {} seconds.".format(round(time.time() - t, 3)))

    reset = requests.post(
        "{}upload/{}".format(host_api, library_uuid), headers=headers, data=payload
    )
    return reset


print(sys.argv[1])
print(
    upload_books(
        "http://localhost:2020/",
        "192350bd-43d1-45b6-ab39-e9142f28e77a",
        "192350bd-43d1-45b6-ab39-e9142f28e77a",
        open(f"{sys.argv[1]}").read(),
    )
)
