import json

from motw_client import (
    add_books,
    add_library,
    bookids,
    calibre_to_json,
    library_on,
    load_books,
    remove_books,
    upload_books,
)

# HOST_API = "http://159.89.224.111"
HOST_API = "http://localhost:2018"
HOST_API_UPLOAD = "http://localhost:2020"
DB_PATH = "/home/m/CalibreLibraries/"


def add_library_and_books(librarian, lsb_url, db_path, library_uuid, library_secret):
    try:
        print(add_library(HOST_API, library_uuid, library_secret).content.decode())
    except Exception as e:
        print("add_library failed: {}").format(e)

    bids = bookids(HOST_API, library_uuid, library_secret)
    books_list = calibre_to_json(library_uuid, library_secret, librarian, db_path)
    rids = set(
        [
            s.split("___")[0]
            for s in list(
                set(bids.json())
                - set(
                    ["{}___{}".format(b["_id"], b["last_modified"]) for b in books_list]
                )
            )
        ]
    )
    # print("RIDS: {}".format(list(rids)))
    if rids:
        reset = remove_books(
            HOST_API, library_uuid, library_secret, json.dumps(list(rids))
        )

        if reset.json()["reset"]:
            # library_off(library_uuid, library_secret)
            library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url, "on")
        else:
            library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url)

    bids = bookids(HOST_API, library_uuid, library_secret)
    sids = set(
        [
            s.split("___")[0]
            for s in list(
                set(
                    ["{}___{}".format(b["_id"], b["last_modified"]) for b in books_list]
                )
                - set(bids.json())
            )
        ]
    )

    print("SIDS: {}".format(len(sids)))
    if sids:
        # reset = add_books(
        upload = upload_books(
            # HOST_API,
            HOST_API_UPLOAD,
            library_uuid,
            library_secret,
            json.dumps([b for b in books_list if b["_id"] in sids]),
            True,
        )
        if upload.ok:
            reset = load_books(HOST_API, library_uuid, library_secret)
        if reset.json()["reset"]:
            # library_off(library_uuid, library_secret)
            library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url, "on")
        else:
            library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url)


libraries = [
    ("anybody", "192350bd-43d1-45b6-ab39-e9142f28e77a", "any body"),
    ("badco", "5268baec-b884-4a75-be84-ed0ca791f465", "Pina Paxton"),
    ("biopolitics", "ee0bdf36-ff42-418b-ac3d-bb983fb96f5a", "Electra Barok"),
    (
        "calamitousannunciation",
        "3693fec5-292d-485e-b93e-b68b1873a3e6",
        "Calamitous Annunciation",
    ),
    ("dubravka", "64bbfb1b-0119-44c8-85d6-9675bec3d21c", "dubravka"),
    ("economics", "211c59b5-f60f-47a7-b148-74c0c892397a", "Charles Harsdorffer"),
    ("feminism", "01d1276a-ab48-4d02-bba4-4a5e832432d2", "Shiyali Ramamrita Krupskaya"),
    ("herman", "72f60870-2e60-402f-9c4c-2809f5842b0b", "Gerard Rozier"),
    ("hortense", "3624df20-00e8-484d-9d7b-41dc2ba89ac3", "George Luis Bataille"),
    ("kok", "60134c32-5c64-49ae-96d3-9e96df7572bb", "Kenneth Jungius"),
    ("marcell", "fac314b6-cfa9-4d2b-99d6-e40a37599cdd", "marcell mars"),
    ("med", "e37b75a2-4313-4f7d-9d98-8cbcb1fdd925", "Eleanor Piade"),
    ("midnightnotes", "19eb09e7-16c8-4e17-ba29-0013bc0ccfab", "Aaron Bataille"),
    ("otpisane", "5c6c45c2-1357-4c97-bccb-3346b7612aea", "Sreten Lešaja"),
    ("praxis", "640890f8-eb43-4ea7-9af6-b53f8fe521d0", "Rudi Petrović"),
    ("quintus", "227f443c-21ae-416e-838d-7cdd12878155", "Quintus"),
    ("slowrotation", "08fd8cc7-7bef-4174-80eb-074f19c04bed", "Slowrotation"),
    ("tamoneki", "157c99c3-a6d4-447e-8724-a05fdeba3be7", "tamo neki"),
    ("outernationale", "a8ffd2bc-b7bc-4d56-97f4-1457710d432a", "outernationale"),
]

for library in libraries:
    # print(library[0])
    print(library)
    librarian = library[2]
    lsb_url = "//{}.memoryoftheworld.org/".format(library[0])
    db_path = "{}{}/metadata.db".format(DB_PATH, library[0])
    library_uuid = library[1]
    library_secret = library[1]
    add_library_and_books(librarian, lsb_url, db_path, library_uuid, library_secret)
